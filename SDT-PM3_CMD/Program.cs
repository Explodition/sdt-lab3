﻿using SDT_PM3_ClassLibrary;
using System;

namespace SDT_PM3_CMD
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter circle radius");
            double circleRadius = double.Parse(Console.ReadLine());
            Console.WriteLine("Enter arc angle (in degrees)");
            double arcAngleInDegrees = double.Parse(Console.ReadLine());

            double segmentArea = CircleSegmentArea.CircleSegmentAreaCount(arcAngleInDegrees, circleRadius);
            Console.WriteLine("The segment area is: " + segmentArea);

            Console.ReadKey();
        }
    }
}
