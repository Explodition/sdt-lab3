using NUnit.Framework;
using SDT_PM3_ClassLibrary;
using System;

namespace SDT_PM3_NUnit
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void Test1()
        {

            const double x = 4;
            const double y = 120;
            const double expect = 9.8;

            double result = CircleSegmentArea.CircleSegmentAreaCount(y, x);
            Assert.AreEqual(expect, result);
        }

        [Test]
        public void Test2()
        {
            const double x = -4;
            const double y = 120;

            Assert.Throws<ArgumentException>(() => CircleSegmentArea.CircleSegmentAreaCount(y, x));
        }
    }
}