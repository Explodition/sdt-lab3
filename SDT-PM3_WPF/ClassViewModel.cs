﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using SDT_PM3_ClassLibrary;

namespace SDT_PM3_WPF
{
    public class ClassViewModel : INotifyPropertyChanged
    {
        private double _arcAngleInDegrees;
        public double ArcAngleDegrees
        {
            get { return _arcAngleInDegrees; }
            set
            {
                _arcAngleInDegrees = value;
                OnPropertyChanged("Result");
            }
        }

        private double _circleRadius;
        public double CircleRadius
        {
            get { return _circleRadius; }
            set
            {
                _circleRadius = value;
                OnPropertyChanged("Result");
            }
        }


        public double Result
        {
            get { return CircleSegmentArea.CircleSegmentAreaCount(ArcAngleDegrees, CircleRadius); }
        }

        public ClassViewModel()
        {
            
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
