﻿using System;
using System.ComponentModel;

namespace SDT_PM3_ClassLibrary
{
    public class CircleSegmentArea : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        //private float arcAngle = new float();
        //private float circleRadius = new float();

        public static double CircleSegmentAreaCount(double arcAngleInDegrees, double circleRadius)
        {
            if (arcAngleInDegrees >= 0 & circleRadius >= 0)
            {
                double circleSegmentArea = Math.Pow(circleRadius, 2) / 2 * ((Math.PI * arcAngleInDegrees / 180) - Math.Sin(arcAngleInDegrees * Math.PI / 180));
                return Math.Round(circleSegmentArea, 1);
            }
            else
            {
                throw new Exception("Radius can't be < 0");
            }
        }
    }


}
